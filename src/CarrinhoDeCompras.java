
import java.util.HashMap;

public class CarrinhoDeCompras {

	private HashMap<Produto, Integer> carrinho = new HashMap<>();
	private static double total = 0;

	public HashMap<Produto, Integer> adicionaProduto(Produto p, int quantidade) {
		if (carrinho.containsKey(p))
			carrinho.put(p, carrinho.get(p) + quantidade);
		else
			carrinho.put(p, quantidade);
		return carrinho;
	}

	public boolean removeProduto(Produto p, int quantidade) {
		if (carrinho.containsKey(p) && carrinho.get(p) > quantidade) {
			carrinho.put(p, carrinho.get(p) - quantidade);
			return true;
		}
		if (carrinho.get(p) == quantidade) {
			carrinho.remove(p);
			return true;
		}
		return false;
	}

	public double getPrecoTotalCarrinho() {
		for (Produto p : carrinho.keySet())
			total += p.getPreco() * carrinho.get(p);
		return total;
	}

	public HashMap<Produto, Integer> getCarrinho() {
		return carrinho;
	}

	public void zerarCarrinho() {
		carrinho.clear();
		total = 0;
	}
}