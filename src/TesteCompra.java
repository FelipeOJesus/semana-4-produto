import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TesteCompra {

	Produto produto;
	CarrinhoDeCompras carrinho;
	ProdutoComTamanho produtoTamanho;

	@Before
	public void inicializador() {
		carrinho = new CarrinhoDeCompras();
		carrinho.zerarCarrinho();
	}

	@Test
	public void adicionarCarrinho() {
		produto = new Produto(1, "Camisa", 10.00);
		assertEquals(1, carrinho.adicionaProduto(produto, 1).get(produto), 0.01);
		assertEquals(1, carrinho.getCarrinho().get(produto), 0.01);
	}
	
	@Test
	public void adicionarOMesmoProdutoAoCarrinho() {
		produto = new Produto(1, "Camisa", 10.00);
		assertEquals(1, carrinho.adicionaProduto(produto, 1).get(produto), 0.01);
		carrinho.adicionaProduto(produto, 1);
		
		assertEquals(2, carrinho.getCarrinho().get(produto), 0.01);
	}

	@Test
	public void removerCarrinho() {
		produto = new Produto(1, "Camisa", 10.00);
		carrinho.adicionaProduto(produto, 2);
		assertTrue(carrinho.removeProduto(produto, 1));
		assertEquals(1, carrinho.getCarrinho().get(produto), 0.01);
	}

	@Test
	public void removerMaisQueQuantidadeCarrinho() {
		produto = new Produto(1, "Camisa", 10.00);
		carrinho.adicionaProduto(produto, 2);
		assertFalse(carrinho.removeProduto(produto, 3));
	}

	@Test
	public void removerUmItem() {
		produto = new Produto(1, "Camisa", 10.00);
		carrinho.adicionaProduto(produto, 2);
		produto = new Produto(2, "Cal�a", 20.00);
		carrinho.adicionaProduto(produto, 2);
		assertTrue(carrinho.removeProduto(produto, 2));
		assertEquals(1, carrinho.getCarrinho().size(), 0.01);
	}

	@Test
	public void valorTotal() {
		produto = new Produto(1, "Camisa", 10.00);
		carrinho.adicionaProduto(produto, 2);
		produto = new Produto(2, "Cal�a", 20.00);
		carrinho.adicionaProduto(produto, 1);
		assertEquals(40, carrinho.getPrecoTotalCarrinho(), 0.01);
	}

	@Test
	public void valorTotalTamanhosDiferentes() {
		produto = new Produto(1, "Camisa", 10.00);
		carrinho.adicionaProduto(produto, 2);
		produtoTamanho = new ProdutoComTamanho(1, "Camisa", 10.00, 2);
		carrinho.adicionaProduto(produtoTamanho, 1);
		assertEquals(30, carrinho.getPrecoTotalCarrinho(), 0.01);
	}

}
