
public class ProdutoComTamanho extends Produto {

	private int tamanho;

	public ProdutoComTamanho(int codigo, String nome, double preco, int tamanho) {
		super(codigo, nome, preco);
		this.tamanho = tamanho;

	}

	@Override
	public int hashCode() {
		int codAuxiliar = 12;
		int result = super.hashCode();
		result = codAuxiliar * result + tamanho;
		return result;
	}

	@Override
	public boolean equals(Object produto) {
		if (!(produto instanceof ProdutoComTamanho))
			return false;
		ProdutoComTamanho produtoAux = (ProdutoComTamanho) produto;

		if (this.hashCode() == produtoAux.hashCode())
			return true;
		else
			return false;
	}

}