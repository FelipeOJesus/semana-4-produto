
public class Produto {

	private String nome;
	private int codigo;
	private double preco;

	public Produto(int codigo, String nome, double preco) {
		this.codigo = codigo;
		this.nome = nome;
		this.preco = preco;
	}

	@Override
	public int hashCode() {
		int codAuxiliar = 12;
		codAuxiliar = codAuxiliar * 1 + codigo;
		return codAuxiliar;
	}

	@Override
	public boolean equals(Object produto) {
		if (!(produto instanceof Produto))
			return false;
		Produto produtoAux = (Produto) produto;

		if (this.hashCode() == produtoAux.hashCode())
			return true;
		else
			return false;
	}

	public double getPreco() {
		return preco;
	}

}